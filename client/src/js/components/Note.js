import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'styles/Note.css';

export const Note = props => (
    <div onClick={props.onClick}
        className={classNames('Note', { 'Note-expanded': props.expanded })}>
        {props.text}
    </div>
)

Note.propTypes = {
    text: PropTypes.string.isRequired,
    expanded: PropTypes.bool,
    onClick: PropTypes.func.isRequired
}