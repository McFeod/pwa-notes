import React from 'react';
import PropTypes from 'prop-types';
import { Note } from 'js/components/Note';
import 'styles/NoteList.css';

export const NoteList = props => {
  const notes = Object.keys(props.notes).map(key => (
    <Note
      key={`note_${key}`}
      onClick={() => props.onSelect(key)}
      expanded={props.selected === key}
      text={props.notes[key]}
    />
  ));
  return (
    <div className='content'>
      {notes.length ? notes : 'Нет записей'}
    </div>
  )
};

NoteList.propTypes = {
  notes: PropTypes.object,
  selected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  onSelect: PropTypes.func.isRequired
};
