import React from 'react';
import PropTypes from 'prop-types';
import 'styles/TopBar.css';
import logo from 'images/icon.svg';

export const TopBar = props => (
  <header className='TopBar'>
    <div className='content'>
      <span className='TopBar-logo'>
        <img className='TopBar-logo-picture' alt='' src={logo} onClick={props.iconClick}/>
        <span className='TopBar-logo-name'>{props.title}</span>
      </span>
      <span className='TopBar-buttons'>
        {props.children}
      </span>
    </div>
  </header>
);

TopBar.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string.isRequired,
  iconClick: PropTypes.func
}

TopBar.defaultProps = {
  title: 'Записи'
}