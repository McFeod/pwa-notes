import React from 'react';
import { LoadingScreen } from 'js/components/LoadingScreen';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NotesScreen from 'js/containers/NotesScreen';
import EditorScreen from 'js/containers/EditorScreen';
import { Page404 } from 'js/components/Page404';
import PropTypes from 'prop-types';
import { PersistGate } from 'redux-persist/integration/react';

export const App = (props) => (
  <Provider store={props.store}>
    <PersistGate loading={<LoadingScreen/>} persistor={props.persistor}>
      <Router>
        <Switch>
          <Route path='/' exact={true} component={NotesScreen} />
          <Route path='/add/' component={EditorScreen} />
          <Route path='/edit/:noteId/' component={EditorScreen} />
          <Route component={Page404} />
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired,
  persistor: PropTypes.object.isRequired
};
