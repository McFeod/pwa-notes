import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import { combineReducers } from 'redux';
import { auth } from 'js/reducers/auth';
import { notes } from 'js/reducers/notes';

const rootPersistConfig = { key: 'root', storage, stateReconciler: autoMergeLevel2, blacklist: ['auth'] };
const authPersistConfig = { key: 'auth', storage, stateReconciler: autoMergeLevel2, whitelist: ['token', 'googleResponse'] };

const rootReducer = persistReducer(
    rootPersistConfig, combineReducers({
        auth: persistReducer(authPersistConfig, auth),
        notes
    })
)

export const configureStore = () => {
    const store = createStore(rootReducer, applyMiddleware(thunk));
    const persistor = persistStore(store);
    return { store, persistor }
};
