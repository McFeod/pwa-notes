import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TopBar } from 'js/components/TopBar';
import FontAwesome from 'react-fontawesome';
import { ROUTER_PROPTYPES } from 'js/prop-types/routes';
import { add, edit } from 'js/actions/notes';
import 'styles/Editor.css';

class EditorScreen extends Component {
  static propTypes = {
    ...ROUTER_PROPTYPES,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    saveNote: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    this.state = { text: props.text };
  }

  handleBack = () => {
    this.props.history.replace('/');
  }

  handleSave = () => {
    if (this.state.text) {
      this.props.saveNote(this.state.text);
      this.handleBack();
    }
  }

  handleChange = event => {
    this.setState({
      text: event.target.value
    })
  }

  render() {
    return ([
      <TopBar key='bar' title={this.props.title} iconClick={this.handleBack}>
        <FontAwesome name='save' onClick={this.handleSave} />
      </TopBar>,
      <div className='page page_fullheight' key='page'>
        <textarea
          autoFocus={true}
          className='content Editor'
          onChange={this.handleChange}
          value={this.state.text}
        />
      </div>
    ])
  }
}


const getNoteId = ownProps => parseInt(ownProps.match.params.noteId, 10);

const mapStateToProps = (state, ownProps) => {
  const noteId = getNoteId(ownProps);
  const text = noteId ? state.notes.items[noteId] : '';
  const title = noteId ? 'Редактирование' : 'Добавление';
  if (typeof text === 'undefined') {
    ownProps.history.push('/404/');
  }
  return { text, title };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  const noteId = getNoteId(ownProps);
  if (noteId) {
    return { saveNote: note => dispatch(edit(noteId, note)) };
  }
  return { saveNote: note => dispatch(add(note)) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorScreen)