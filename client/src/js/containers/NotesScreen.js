import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NoteList } from 'js/components/NoteList';
import { deleteNote } from 'js/actions/notes';
import { TopBar } from 'js/components/TopBar';
import { ROUTER_PROPTYPES } from 'js/prop-types/routes';
import 'font-awesome/css/font-awesome.min.css';
import FontAwesome from 'react-fontawesome';
import { canBeAuthorized, login } from 'js/actions/auth';

class NotesScreen extends Component {
  static propTypes = {
    ...ROUTER_PROPTYPES,
    deleteNote: PropTypes.func.isRequired,
    notes: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      selected: null
    }
  }

  handleAdd = () => {
    this.props.history.push('/add/');
  }

  handleDelete = () => {
    this.props.deleteNote(this.state.selected);
    this.setState({ selected: null });
  }

  handleEdit = () => {
    this.props.history.push(`/edit/${this.state.selected}/`);
  }

  handleSelect = id => {
      this.setState({ selected: this.state.selected === id ? null : id });
  }

  render() {
    return [
      <TopBar key='bar'>
        {canBeAuthorized(this.props.auth) ?
          <FontAwesome name='refresh' onClick={this.props.login} />
          : null}
        {this.state.selected && [
          <FontAwesome name='trash' key='delete' onClick={this.handleDelete} />,
          <FontAwesome name='edit' key='edit' onClick={this.handleEdit} />
        ]}
        <FontAwesome name='plus-circle' onClick={this.handleAdd} />
      </TopBar>,
      <div className='page' key='page'>
        <NoteList
          notes={this.props.notes}
          onSelect={this.handleSelect}
          selected={this.state.selected}
        />
      </div>
    ];
  }
}

export default connect(
  state => ({ notes: state.notes.items, auth: state.auth }),
  { deleteNote, login }
)(NotesScreen)