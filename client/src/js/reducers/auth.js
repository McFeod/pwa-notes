import { combineActions, handleActions } from 'redux-actions';
import { setAuthStatus, setupGoogleOauth, saveToken } from 'js/actions/auth';

const defaultState = {};

export const auth = handleActions({
  [combineActions(setupGoogleOauth, setAuthStatus, saveToken)]:
    (state, { payload }) => ({ ...state, ...payload })
}, defaultState);
