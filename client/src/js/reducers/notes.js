import { handleActions } from 'redux-actions';
import { add, edit, deleteNote } from 'js/actions/notes';

const defaultState = { noteCounter: 0, items: {} };

export const notes = handleActions({
    [add]: (state, { payload: { text } }) => ({ 
        ...state, 
        noteCounter: state.noteCounter + 1,
        items: { ...state.items, [state.noteCounter + 1]: text } 
    }),
    [edit]: (state, { payload: { id, text } }) => ({ ...state, items: { ...state.items, [id]: text } }),
    [deleteNote]: (state, { payload: { id } }) => ({ 
        ...state, items: Object.keys(state.items).reduce((accumulator, key) => (
            key === id ? accumulator : {...accumulator, [key]: state.items[key]}
        ), {})
    })
}, defaultState);
