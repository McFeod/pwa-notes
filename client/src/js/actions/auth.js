import { createAction } from 'redux-actions';

export const setupGoogleOauth = createAction('SETUP_GOOGLE_AUTH', googleAuth => ({ googleAuth }));
export const setAuthStatus = createAction('SET_AUTH_STATUS', isSending => ({ isSending }));
export const saveToken = createAction('SAVE_TOKEN', (googleResponse, token) => ({ googleResponse, token }));

const clientId = "419399771761-1t0i4b7i7k6aupr1ivdpvveqraujsrst.apps.googleusercontent.com";

const initOauth2 = async (dispatch) => {
  await new Promise((resolve, reject) => window.gapi.load('auth2', resolve, reject));
  const googleAuth = await window.gapi.auth2.init({ client_id: clientId});
  dispatch(setupGoogleOauth(googleAuth));
};

export const initGoogleAuth = dispatch => {
  if (window.gapi) {  // cached
    initOauth2(dispatch)
  }
  return () => initOauth2(dispatch);
}

export const canBeAuthorized = auth => auth.googleAuth && !auth.token && !auth.isSending;

const postJSON = (url, json) => fetch(url, {
  method: 'post',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(json)
});

const authByGoogle = async googleResponse => {
  try {
    const response = await postJSON('/login/google/', { token: googleResponse.Zi.id_token });
    return (await response.json()).token;
  } catch(e) {
    console.log(e);
    return;
  }
} 

export const login = () => async (dispatch, getState) => {
  dispatch(setAuthStatus(true));
  const { auth } = getState();
  try {
    const googleResponse = await auth.googleAuth.signIn();
    dispatch(saveToken(googleResponse, await authByGoogle(googleResponse)));
  } finally {
    dispatch(setAuthStatus(false));
  }
}