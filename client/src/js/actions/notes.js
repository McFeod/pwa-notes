import { createAction} from 'redux-actions';

export const add = createAction('NOTE_ADD', text => ({ text }));
export const edit = createAction('NOTE_EDIT', (id, text) => ({ id, text }));
export const deleteNote = createAction('NOTE_DELETE', id => ({ id }));
