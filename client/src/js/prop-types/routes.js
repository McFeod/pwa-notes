import PropTypes from 'prop-types';

export const ROUTER_PROPTYPES = {
    match: PropTypes.shape({
        path: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        isExact: PropTypes.bool.isRequired,
        params: PropTypes.object.isRequired
    }).isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
}