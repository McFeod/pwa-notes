import React from 'react';
import ReactDOM from 'react-dom';
import 'styles/index.css';
import { App } from 'js/App';
import registerServiceWorker from 'js/registerServiceWorker';
import { configureStore } from 'js/store';
import { initGoogleAuth } from 'js/actions/auth';

const store = configureStore();
ReactDOM.render(<App { ...store }/>, document.getElementById('root'));
window.initGoogle = initGoogleAuth(store.store.dispatch);
window.store = store.store;

registerServiceWorker();
