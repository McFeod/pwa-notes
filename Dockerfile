FROM node:8.11-alpine

RUN adduser -D myuser
RUN mkdir /app
RUN chown myuser /app
USER myuser

WORKDIR /app
COPY package.json /app/package.json
RUN npm install

COPY server /app/server
COPY client/build /app/static
COPY production.json /app/config.json

CMD ["node", "--experimental-modules", "server/app.mjs"]
