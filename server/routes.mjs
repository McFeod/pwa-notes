import { googleAuthView } from './auth';
import { noteListView } from './notes';

export const setupRoutes = app => {
    app.post('/login/google/', googleAuthView);
    app.get('/notes/', app.loginRequired, noteListView);
}