import mongo from 'then-mongo';


export class MongoRepository {
  async init(config) {
    this.db = await mongo(config.url);
  }

  async performLogin(googleId) {
    const query = { googleId };
    let user = await this.db.collection('users').findOne(query);
    if (!user) {
      user = await this.db.collection('users').insert(query);
    }
    return user._id;
  }

  async getUser(id) {
    return await this.db.collection('users').findOne({ _id: new mongo.ObjectID(id) });
  }
}
