export const noteListView = (req, res) => {
  res.json(req.user.notes || []);
};
