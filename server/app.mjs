import Express from 'express';
import { setupRoutes } from './routes';
import { setupMiddlewares } from './middlewares';
import config from '../config.json';
import { MongoRepository } from './database';

const setup = async config => {
  const app = Express();
  app.config = config;
  app.mongo = new MongoRepository();
  await app.mongo.init(config.mongo);
  setupMiddlewares(app);
  setupRoutes(app);
  const port = parseInt(process.env.PORT, 10) || 5000;
  app.listen(port, () => console.log(`listening on port ${port}`));
}

setup(config);
