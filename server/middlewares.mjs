import logger from 'morgan';
import bodyParser from 'body-parser';
import express from 'express';
import path from 'path';
import secure from "express-force-https";
import passport from 'passport'
import passportJWT from 'passport-jwt';


const authMiddleware = repository => async (payload, next) => {
    try {
        const user = await repository.getUser(payload.userId);
        next(null, user);
    } catch (e) {
        next(null, false);
    }
}

export const setupMiddlewares = app => {
    // app.use(secure);
    passport.use(new passportJWT.Strategy(
        {
            jwtFromRequest: passportJWT.ExtractJwt.fromHeader('authorization'),
            secretOrKey: app.config.jwtSecret
        },
        authMiddleware(app.mongo)
    ));
    app.use(passport.initialize());
    app.loginRequired = passport.authenticate('jwt', { session: false });
    app.use(bodyParser.json());
    app.use(logger(app.config.logger));
    app.use(express.static(path.join(path.resolve(), app.config.static)))
}