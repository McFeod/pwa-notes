import google from 'google-auth-library';
import jwt from 'jsonwebtoken';

async function verifyByGoogle(clientId, token) {
    const client = new google.OAuth2Client(clientId);
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: clientId,
    });
    const payload = ticket.getPayload();
    return payload['sub'];
}

const generateToken = async (googleId, secret, db) => {
    const payload = {
        userId: await db.performLogin(googleId),
        signedIn: new Date()
    };
    return jwt.sign(payload, secret);
}

export const googleAuthView = async (req, res) => {
    if (req.body && req.body.token) {
        try {
            const { config, mongo } = req.app;
            const googleId = await verifyByGoogle(config.oauth.google.clientId, req.body.token);
            res.json({ token: await generateToken(googleId, config.jwtSecret, mongo) });
        } catch (e) {
            console.log(e);
            res.status(401);
            res.json({ error: 'google auth failed'});
        }
    } else {
        res.status(400);
        res.json({ error: 'token not provided' })
    }
}
